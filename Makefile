TEXFILE=ferrero_cv.tex
PDFREADER=open #xpdf

PDFFILE:=$(TEXFILE:.tex=.pdf) # rename file.tex -> file.pdf
BIBFILE:=$(TEXFILE:.tex="")   # rename file.tex -> file

all:
	#pdflatex $(TEXFILE)
	xelatex $(TEXFILE)

pdflatex:
	pdflatex $(TEXFILE)

xelatex:
	xelatex $(TEXFILE)

show:
	$(PDFREADER) $(PDFFILE) &

clean:
	rm -f *.aux *.idl *.idx *.ilg *.ind *.lof *.log *.lot *.toc *.blg *.bbl *.out

git:
	git commit -am 'updated cv'
	git push 

dbox:
	cp $(PDFFILE) $(TEXFILE) ~/Dropbox/Public/cv/

.PHONY: all pdflatex xelatex show clean git dbox